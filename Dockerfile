FROM python:3.6.5

ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip

# install pip modules (own filesystem layer)
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

# copy our app
COPY app /usr/src/app

# some info for the callers
EXPOSE 8000
EXPOSE 80

#ENV API_KEY="2RvoXmGP4BWBmSFINaZriov3WFOpEzZU"
#ENV RESOURCE_AMBASSADOR="http://lb.internal.matrix-computer.com/resource-ambassador"
#ENV INTEGRATION_TESTS=1
#ENV UNIT_TESTS=1

# run our service
#CMD ["python","/src/app/main.py"]
WORKDIR "/usr/src/app"
RUN python manage.py migrate
CMD ["python", "main.py"]

